package trpg;
//キャラクターの能力値を決定するクラス
import java.util.LinkedHashMap;
import java.util.Map;

public class CharacterCreation {
	private String playerName;
	private int strength;
	private int constitution;
	private int size;
	private int dexterity;
	private int appearance;
	private int intelligence;
	private int power;
	private int education;
	private int luck;
	private int move;
	private int hp;
	private int mp;
	private int damageBonus;
	private int build;
	private int sanPoint;

	//キャラクターの能力値をランダムで決定するメソッド
	public Map<String, Integer> playerCreat(){
		System.out.println("探索者の名前を入力してください");
		System.out.print("探索者の名前：");
		String playerName=new java.util.Scanner(System.in).nextLine();
		boolean playerMakeComplete=false;
		while(playerMakeComplete==false){
			//ここからキャラクターの能力値をランダム決定するコード
			int strength=(new java.util.Random().nextInt(16)+3)*5;
			int constitution=(new java.util.Random().nextInt(16)+3)*5;
			int size=(new java.util.Random().nextInt(11)+8)*5;
			int dexterity=(new java.util.Random().nextInt(16)+3)*5;
			int appearance=(new java.util.Random().nextInt(16)+3)*5;
			int intelligence=(new java.util.Random().nextInt(11)+8)*5;
			int power=(new java.util.Random().nextInt(16)+3)*5;
			int education=(new java.util.Random().nextInt(11)+8)*5;
			int hp=(constitution+size)/10;
			int mp=power/5;
			int sanPoint=power;
			int luck=(new java.util.Random().nextInt(16)+3)*5;
			if(size>strength&&size>dexterity){
				move=7;
			}else if(size<=strength||size<=dexterity){
				move=8;
			}else{
				move=9;
			}
			if(strength+size<65){
				damageBonus=-2;
				build=-2;
			}else if(strength+size<85){
				damageBonus=-1;
				build=-1;
			}else if(strength+size<125){
				damageBonus=0;
				build=0;
			}else if(strength+size<165){
				damageBonus=1;
				build=1;
			}else{
				damageBonus=2;
				build=2;
			}
			//ランダム決定された能力値を表示するコード
			System.out.println("耐久力(HP):"+hp);
			System.out.println("マジックポイント(MP)"+mp);
			System.out.println("正気度(SAN値)"+sanPoint);
			System.out.println("幸運"+luck);
			System.out.println("移動率(MOV)"+move);
			System.out.println("ダメージ・ボーナス(1の場合1D4、2の場合1D6)"+damageBonus);
			System.out.println("ビルド"+build);
			System.out.println("STR(筋力):"+strength);
			System.out.println("CON(体力):"+constitution);
			System.out.println("SIZ(体格):"+size);
			System.out.println("DEX(敏捷性):"+dexterity);
			System.out.println("APP(外見):"+appearance);
			System.out.println("INT(知性):"+intelligence);
			System.out.println("POW(精神力):"+power);
			System.out.println("EDU(教育):"+education);
			//マップに能力値を入力するコード
			Map<String,Integer> playerStatus=new LinkedHashMap<>();
			playerStatus.put("耐久力",hp);
			playerStatus.put("マジックポイント",mp);
			playerStatus.put("正気度",sanPoint);
			playerStatus.put("幸運",luck);
			playerStatus.put("移動率",move);
			playerStatus.put("ダメージ・ボーナス",damageBonus);
			playerStatus.put("ビルド",build);
			playerStatus.put("STR",strength);
			playerStatus.put("CON",constitution);
			playerStatus.put("SIZ",size);
			playerStatus.put("DEX",dexterity);
			playerStatus.put("APP",appearance);
			playerStatus.put("INT",intelligence);
			playerStatus.put("POW",power);
			playerStatus.put("EDU",education);
			System.out.println("");
			System.out.println("この能力値で決定しますか?");
			System.out.println("1:はい 2:いいえ");
			int choice=new java.util.Scanner(System.in).nextInt();
			if(choice==1){
				playerMakeComplete=true;
				System.out.println("この能力値で決定します");
				return playerStatus;
			}else if(choice==2){
				System.out.println("能力値を再決定します");
			}else{
				continue;
			}
		}
		return null;
	}
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	public int getConstitution() {
		return constitution;
	}
	public void setConstitution(int constitution) {
		this.constitution = constitution;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getDexterity() {
		return dexterity;
	}
	public void setDexterity(int dexterity) {
		this.dexterity = dexterity;
	}
	public int getAppearance() {
		return appearance;
	}
	public void setAppearance(int appearance) {
		this.appearance = appearance;
	}
	public int getIntelligence() {
		return intelligence;
	}
	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getEducation() {
		return education;
	}
	public void setEducation(int education) {
		this.education = education;
	}
	public int getLuck() {
		return luck;
	}
	public void setLuck(int luck) {
		this.luck = luck;
	}
	public int getMove() {
		return move;
	}
	public void setMove(int move) {
		this.move = move;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getMp() {
		return mp;
	}
	public void setMp(int mp) {
		this.mp = mp;
	}
	public int getDamageBonus() {
		return damageBonus;
	}
	public void setDamageBonus(int damageBonus) {
		this.damageBonus = damageBonus;
	}
	public int getBuild() {
		return build;
	}
	public void setBuild(int build) {
		this.build = build;
	}
	public int getSanPoint() {
		return sanPoint;
	}
	public void setSanPoint(int sanPoint) {
		this.sanPoint = sanPoint;
	}
}
