package trpg;

import java.util.Map;

public class StatusWindow {

	//キャラクターの能力値を確認するメソッド
	CharacterCreation characterCreation=new CharacterCreation();
	public void showStatus(Map<String,Integer> statusArray){
		for(String statusName:statusArray.keySet()){
			int value=statusArray.get(statusName);
			System.out.println(statusName+":"+value);
		}

	}

}
