package trpg;

import java.util.Map;

public class Main {

	public static void main(String[] args) {
		System.out.println("探索者を作成します");
		CharacterCreation characterCreation=new CharacterCreation();
		StatusWindow statusWindow=new StatusWindow();
		Dice dice=new Dice();
		Map<String,Integer> statusArray=characterCreation.playerCreat();
		boolean useProgram=true;
		while(useProgram==true){
			System.out.println("何を行いますか?");
			System.out.println("1:能力値の確認　2:ダイスを振る　3:プログラムの終了");
			int choice=new java.util.Scanner(System.in).nextInt();
			if(choice==1){
				statusWindow.showStatus(statusArray);
			}else if(choice==2){
				dice.useDice(statusArray);
			}else if(choice==3){
				useProgram=false;
				System.out.println("プログラムを終了します");
			}else{
				System.out.println("もう一度選択してください");
			}
		}

	}

}
