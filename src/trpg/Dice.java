package trpg;

import java.util.Map;

public class Dice {
	//ダイスを振って結果を表示するメソッド
	public void useDice(Map<String,Integer> statusArray){
		boolean useDice=true;
		while(useDice==true){
			System.out.println("どの能力でダイスを振るか選択してください");
			String statusName=new java.util.Scanner(System.in).nextLine();
			try{
			int statusPoint=statusArray.get(statusName);
			int dice = new java.util.Random().nextInt(100)+1;
			if(dice==1){
				System.out.println("あなたのダイス結果は"+dice+"で、決定的な成功です。");
			}else if(statusPoint<50&&dice>95){
				System.out.println("あなたのダイス結果は"+dice+"で、致命的な失敗です。");
			}else if(statusPoint>=50&&dice>99){
				System.out.println("あなたのダイス結果は"+dice+"で、致命的な失敗です。");
			}else if(dice<statusPoint){
				System.out.println("あなたのダイス結果は"+dice+"で、成功です。");
			}else if(dice>statusPoint){
				System.out.println("あなたのダイス結果は"+dice+"で、失敗です。");
			}
			System.out.println("再びダイスを振りますか?");
			System.out.println("1:はい　2:いいえ");
			int choice=new java.util.Scanner(System.in).nextInt();
			if(choice==1){
			}else if(choice==2){
				useDice=false;
			}else{
				System.out.println("もう一度選択してください");
			}
			}catch(Exception e){
				System.out.println("入力した文字列が間違っています");
				continue;
			}
		}
	}
}
